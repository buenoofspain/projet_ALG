#! /bin/bash/env python
# -*- coding: utf-8 -*-

'''
@Subject: Projet ALG
@Authors: Aurélie Nicolas and Nicolas Guillaudeux
@Master Bio-informatique
@Program: Python 3.6.2

Command line test:
>>> python3 mapper.py -c contigs_results_k21.fasta -g mysterious_reference.fasta
'''

import tools_karkkainen_sanders as tks
from DynamicMatrix import *
import argparse

class Mapper:
    '''
    Checking contigs contain in a fasta file with comparison with a reference genome in fasta file format.

    Mapper parameter:
        -   k: Seed size.

    Input:
    @contig: contig in contigs file obtained by the contiger script, in fasta format.
    @genome_file: file with the reference genome, in fasta format.

    Output:
    @checking: results of the checking test validation is in tabulation format (.quality file) present by four columns : 
        -   Name of the tested contig as defined in the input fasta file;
        -   Contig size (in bp);
        -   Percentage of differences (number of substitutions and indels divided by the size of the contig * 100).
        -   Approximate position of beginning of alignment of the contig on the reference genome.
    '''
    def __init__(self, T, SA, k):
        '''
		A function to initialize necessary parameters for the Mapper class.
		'''
        self.T = T                  #Nucleotide sequence
        self.SA = SA                #Suffix array
        self.lT = len(T)            #Nucleotide sequence size
        self.lSA = len(SA)          #Suffixe array size
        self.k = k                  #Seed size

    def GET_BWT(self):
        '''
        Give the Burrow-Wheeler Transformation with a text T and
        a list of suffix in the alphabetic order for a position: SA[i]
        
        Input:
        @self.T: Nucleotide sequence
        @self.SA: A suffix list in the alphabetic order
        @self.lSA: SA size

        Output:
        @bwt : the Burrow Burrow-Wheeler Transformation which index the reference genome
        '''
        bwt = ""
        for i in range(self.lSA):
            if self.SA[i] > 0:
                bwt += self.T[self.SA[i]-1]
            else :
                bwt += "$"
        return bwt

    def GET_N(self, BWT):
        '''
        A function that give a dictionnary of symboles with their number.

        Input:
        @BWT: the Burrow Burrow-Wheeler Transformation.

        Output:
        @Na: a dictionnary with symbol correspondance.
        '''
        BWT = BWT.upper()
        Na = {}
        Na["$"], Na["A"], Na["C"], Na["G"], Na["T"] = 0, 0, 0, 0, 0
        for i in range(len(BWT)):
            Na[BWT[i]] += 1
        return Na

    def GET_R(self, BWT):
        '''
        A function that give the sequence rank.

        Input:
        @BWT: the Burrow Burrow-Wheeler Transformation.

        Output:
        @rank: rank of the sequence in the BWT.
        '''
        Na={}
        Na["$"], Na["A"], Na["G"], Na["C"], Na["T"] = 0, 0, 0, 0, 0
        rank=[0]*len(BWT)
        for i in range(len(BWT)):
            Na[BWT[i]]+=1
            rank[i]=Na[BWT[i]]
        return rank

    def LF(self, alpha, k, Nalpha):
        '''
        A function that give position alpha in BWT.

        Input:
        @alpha: alpha character whose position is sought in the lexicographic order.
        @k: seed size.
        @Nalpha: liste with sequence in lexicographic order.

        Output:
        Position of the desired character.
        '''
        l = k-1
        if alpha == "$": return 0
        if alpha == "A": return l + Nalpha["$"]
        if alpha == "C": return l + Nalpha["$"] + Nalpha["A"]
        if alpha == "G": return l + Nalpha["$"] + Nalpha["A"] + Nalpha["C"]
        if alpha == "T": return l + Nalpha["$"] + Nalpha["A"] + Nalpha["C"] + Nalpha["G"]
    
    def P_exists_in_T(self, P, BWT, R, Nalpha, SA):
        '''
        A function that check that pattern sequence(P) is in genome sequence.

        Input:
        @P: the pattern tested.
        @BWT: the Burrow-Wheeler Transformation.
        @R: the rank.
        @Nalpha: liste with sequence in lexicographic order.
        @SA: A suffix list in the alphabetic order

        Output:
        Position of the pattern in the genome indexed in the BWT.
        '''
        P = P[::-1]
        i=0
        j=len(BWT)-1
        for position_Q in range(len(P)-1):
            current_char = P[position_Q]
            list_possible = list()
            for x in range(i, j+1):
                if BWT[x] == current_char: 
                    if x not in list_possible:
                        list_possible.append(x)
            if len(list_possible) == 0: return False
            new_i = min(list_possible)
            new_j = max(list_possible)
            i = Mapper.LF(self, current_char, R[new_i], Nalpha)
            j = Mapper.LF(self, current_char, R[new_j], Nalpha)
        index_position = list()
        for index in range(i, j+1):
            index_position.append(SA[index])
        return index_position

    def cut_me(self, sequence):
        '''
        A function that cuts sequence in kmer of size k.

        Input:
        @sequence: sequence that is cut in k-mer
        @k: seed size.

        Output:
        List of generated kmers.
        '''
        list_kmers = list()
        sequence = sequence.rstrip().upper()
        for i in range(len(sequence)-self.k+1):
            kmer = sequence[i:i+self.k]
            list_kmers.append(kmer)
        if len(list_kmers) > 0 : return list_kmers

if __name__ == '__main__':
    '''
    Main fuction to execute the project.
    ''' 
    ### Parser:
    parser = argparse.ArgumentParser(prog = "mapper.py")

    ### Optional input parameters
    # Optionnal argument for seed size.
    parser.add_argument("-k", "--len_k", dest = "k", metavar = "SEED_SIZE", type = int, default = 31, help = "Seed size, by default is 31 bp. The format is an integer.")
    
    ### Input required parameters
    # Required parameters for file with contigs from contiger script and file with reference genome.
    parser.add_argument("-c", "--contig", dest = "contig_file", metavar = "CONTIGS_FASTA_FILE", type = str, help = "contig in contigs file obtained by contiger script, in fasta format", required = True)
    parser.add_argument("-g", "--genome", dest = "genome_file", metavar = "GENOME_FASTA_FILE", type = str, help = "file with reference genome, in fasta format.", required = True)

    ## Args definition
    args = parser.parse_args()

    name_output_file = args.contig_file[:-5] + "quality"
    with open(name_output_file, 'w') as output_file:    # To write a output file.

        with open(args.genome_file, 'r') as genome:     # To open the genome file.
            lines = genome.readlines()                  # To read the genome file.
            print("\t>>> Read genome file: OK")
            for line in lines:
                if line.startswith(">"): continue       # We do not consider the genome name.
                line = line.rstrip()
                line += "$"                             # We add the "$" symbol at the end of the genome sequence.
                sa = tks.simple_kark_sort(line)[:-3]    # To define the suffix array for the genome.
                mapping = Mapper(line, sa, args.k)      # To define the Mapper class for the genome.
                bwt = mapping.GET_BWT()                 # To define the BWT.
                rank = mapping.GET_R(bwt)               # To define the rank.
                n = mapping.GET_N(bwt)                  # To define the symbol dictionnary.

                with open(args.contig_file, 'r') as reads:      # To open the contigs file.
                    lines_contig = reads.readlines()            # To read the contigs file.
                    print("\t>>> Read contigs file: OK")
                    for line_contig in lines_contig:
                        if line_contig.startswith(">"):         # We define the name of contig for the output file in the name_contig variable.
                            name_contig = line_contig[2:]
                            continue 
                        length_contig = len(line_contig.rstrip())               # To give the contig size.
                        cut = mapping.cut_me(line_contig)                       # To cut our contig in kmers.
                        sa_read = tks.simple_kark_sort(line_contig)             # To define the suffix array for the contig.
                        mapping_contig = Mapper(line_contig, sa_read, args.k)   # To define the Mapper class for the contig.
                        calcul_score = 0                                        # Defines the mapping score variable.
                        calcul_score_list = list()                              # Defines the mapping scores for all alignement.
                        position_genome_and_score = dict()                      # To class score alignement and position genome in a dictionnary.
                        for kmer in cut:                                        # To test every kmers
                            index_position_on_genome = mapping.P_exists_in_T(kmer, bwt, rank, n, sa)                    # Defines the index of the kmer in the genome.
                            index_position_on_contig = mapping_contig.P_exists_in_T(kmer, bwt, rank, n, sa)             # Defines the index of the kmer in the contig.
                            if index_position_on_genome != False or index_position_on_contig != False:                  # If the position is not null
                                for pos_genome in index_position_on_genome:                                             # Tests every index
                                    if pos_genome >len(line_contig)-args.k and pos_genome <= len(line_contig)-args.k:   # Condition : no out of range at the beginning and at the end of the genome sequence.
                                        pos_start = pos_genome - len(line_contig) - args.k                              # Defines the start position.
                                        pos_stop = pos_genome + len(line_contig) + args.k                               # Defines the stop position.
                                    elif pos_genome > len(line_contig)-args.k:                                          # Condition : if at the end of the genome sequence
                                        pos_start = pos_genome - len(line_contig) + args.k-1                            # Defines the start position
                                        pos_stop = len(line)-1                                                          # Defines the stop position
                                    else:                                                       # Condition: middle of the genome sequence
                                        pos_start = pos_genome                                  # Defines the start position
                                        pos_stop = pos_genome + len(line_contig) + args.k       # Defines the stop position
                                    seq_genome = line[pos_start:pos_stop]                       # Defines the sequence genome tested at the start and the stop position.
                                    seq_contig = line_contig.rstrip()
                                    dm = DynamicMatrix(seq_contig, seq_genome)                  # To define the dynamic matrix for the contig sequence and the genome sequence tested.
                                    dm.initSemiGlobal()                                         # To initialize the matrix with semi-global condition
                                    dm.fillSemiGlobal()                                         # To fill the matrix with the semi-global alignment.
                                    calcul_score = dm.diffence_score(dm.semiGlobalAln())        # To calcul the alignment score.
                                    calcul_score_list.append(calcul_score)                      # To add this score in the list.
                                    position_genome_and_score[pos_genome] = calcul_score        # And to add this score and its position in the dictionnaty.
                        if len(calcul_score_list) != 0:                                         # If the score list is not null: define the score by the minimum score.
                            calcul_score = min(calcul_score_list)
                            ref_pos_genome = [key for key,value in position_genome_and_score.items() if value == calcul_score]
                            ref_pos_genome = min(ref_pos_genome)                                # Defines the position approximative in the genome sequence.
                        else:                                                                   # If the score list is nul: defines a score null for the identity or à 100% score for the difference and a non attribute position in the genome sequence.
                            calcul_score = 100.0
                            ref_pos_genome = "NA"
                        # To write the result in the output file
                        output_file.write(str(name_contig.rstrip())+'\t'+str(length_contig)+'\t'+str(calcul_score)+'\t'+str(ref_pos_genome)+"\n")
                    print("\t>>> Quality file generation: OK")

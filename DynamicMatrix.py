#! /bin/bash/env python
# -*- coding: utf-8 -*-

'''
@Subject: Projet ALG
@Authors: Aurélie Nicolas and Nicolas Guillaudeux
@Master Bio-informatique
@Program: Python 3.6.2
'''

class DynamicMatrix:
    """
    This class stores a matrix |S|x|T| (|S|+1 lines and |T1 columns). 
    Sequences defines with semi-global alignment functions.

    Matrix parameters:
    -   match: match score;
    -   mismatch: mismatch score;
    -   gap: gap score;
    -   matrix: the initialize matrix;
    -   lS: size of the sequence S;
    -   lT: size of the sequence T.

    Input File:
    @S: Target sequence tested.
    @T: Genome sequence tested.
    """
    
    def __init__(self, S, T, match = 0, mismatch = -1, gap = -1):#, scoreSystem):
        ''' 
        A fonction defines and stores initial values.
        '''
        if len(S) < len(T):             #T must contain the longest sequence
            self.S = S
            self.T = T
            self.lS = len(S)            #S sequence size
            self.lT  = len(T)           #T sequence size
        else:
            self.S = T
            self.T = S
            self.lS = len(T)
            self.lT  = len(S)
        self.gap = gap                  #Score for a gap
        self.match = match              #Score for a match
        self.mismatch = mismatch        #Score for a mismatch
        self.matrix = [[0 for j in range(len(T)+1)] for i in range(len(S)+1)]       #Matrix |S|x|T|
    
    def score(self, C1, C2):
        '''A function give a score to two letters alignment.

        Input:
        @C1: Character of first sequence
        @C2: Character of second sequence
        
        Output:
        @match: match score.
        @mismatch: mismatch score.
        '''
        if C1==C2:
            return self.match       #Two characters match, we return a match score
        else:
            return self.mismatch    #Two characters don't match, we return a mismatch score
            
    def initSemiGlobal(self):
        '''
        A function initiates the first row and first column for semi-global alignment.

        Input:
        @lS: sequence S size.
        @lT: sequence T size.
        @matrix: the matrix.
        @gap: gap score.

        Output:
        Initialize first row and first column for semi-global alignment.
        '''
        if self.lS < self.lT:           #Initiates the first row with gap score
            for i in range(1, self.lS+1):
                self.matrix[i][0] = self.matrix[i-1][0]+self.gap
        else:                           #Initiates the first column with gap score
            for j in range(1, self.lT+1): 
                self.matrix[0][j] = self.matrix[0][j-1]+self.gap

    def fillSemiGlobal(self):
        '''
        A function that fills the matrix with the better score.

        Input:
        @lS: sequence S size.
        @lT: sequence T size.
        @matrix: the matrix.
        @gap : gap score.
        @S: sequence S.
        @T: sequence T.

        Output:
        Matrix complete by a semi-global alignment.
        '''
        for i in range(1, self.lS+1):
            for j in range(1, self.lT+1):
                self.matrix[i][j] = max(self.matrix[i-1][j]+self.gap, 
                    self.matrix[i][j-1]+self.gap,
                    self.matrix[i-1][j-1]+self.score(self.S[i-1], self.T[j-1]))

    def semiGlobalAln(self):
        '''
        A function that gives semi global alignment of two nucleotide sequences.

        Input:
        @T: sequence T.
        @S: sequence S.
        @lS: sequence S size.
        @matrix : the matrix.
        @score(): to give the score.
        @gap: gap score.

        Output:
        @alnS: sequence S alignment.
        @alnT: sequence T alignment.
        '''
        alnS = ""           #Sequence containing S sequence to align.
        alnT = ""           #Sequence containing T sequence to align
        i = self.lS         #Contains last row's value.
        j = self.matrix[i][0:].index(max(self.matrix[i]))   #Contains last row's maximum index.
        while i>0 and j>0:
            if (self.matrix[i][j] == self.matrix[i-1][j-1] + self.score(self.S[i-1],self.T[j-1])):      #Alignment coming from the diagonal
                alnS = self.S[i-1] + alnS
                alnT = self.T[j-1] + alnT
                i -= 1
                j -= 1
            elif self.matrix[i][j] == self.matrix[i-1][j] + self.gap:                                   #Alignment coming from the left box
                alnS = self.S[i-1] + alnS
                alnT = "-" + alnT
                i -= 1                
            else:                           #Alignment comming from the top box
                alnS = "-" + alnS
                alnT = self.T[j-1] + alnT
                j -= 1
        return alnS, alnT

    def diffence_score(self, results_alignment):
        '''
        A function contains percentage difference in score between the two sequences.

        Input:
        @results_alignment: alnS and alnT: S and T alignments from the semi-global alignment.

        Output:
        The score from the alignment. It is based on the difference percentage.
        '''
        alnS, alnT =results_alignment
        score = 0
        for i in range(len(alnS)):
            if alnT[i] == alnS[i]:          #Add 1 at score if two characters are aligned.
                score += 1
        if len(alnS) == 0: 
            return 0
        else : 
            return((1-(score/len(alnS)))*100)

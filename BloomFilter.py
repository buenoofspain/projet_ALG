#! /bin/bash/env python
# -*- coding: utf-8 -*-

'''
@Subject: Projet ALG
@Authors: Aurélie Nicolas and Nicolas Guillaudeux
@Master Bio-informatique
@Program: Python 3.6.2
'''
import math
from math import *

char2bitstr={"A":"00","C":"01","G":"10","T":"11"}

def kmer2hash(kmer,seed):
    '''
    A function that generates a hash table.
    
    Input:
    @kmer: takes as input a kmer.
    @seed: it also takes a seed to know hash function number.
    
    Output:
    @hash: return a hash table.
    '''
    key=int('0b' + ''.join([char2bitstr[c] for c in kmer]), 2)
    hash=seed
    hash ^= (hash <<  7) ^  key * (hash >> 3) ^ (~((hash << 11) + (key ^ (hash >> 5))));
    hash = (~hash) + (hash << 21);
    hash = hash ^ (hash >> 24);
    hash = (hash + (hash << 3)) + (hash << 8);
    hash = hash ^ (hash >> 14);
    hash = (hash + (hash << 2)) + (hash << 4);
    hash = hash ^ (hash >> 28);
    hash = hash + (hash << 31);
    return hash

class BitArray:
    '''
    This class contains functions that generate a array of n bits.

    Input:
    @size: array size in bits.
    '''
    def __init__(self, size):
        '''
        This initialization function of class that allow to create a array of size bits. 
        Size is the size passed in argument.
        '''
        self.size=size
        self.array=bytearray(1+size//8) #We add 1 to avoid filling problem the hash table.

    def __str__(self):
        '''
        This function that print hash table.
        '''
        a=""
        for i in self.array:
            print(i)
            a+=str(bin(i))
        return a
    
    def set_i(self,i):
        '''
        This function positions the i-th bit of array at 1 to signify adding our string to the array.
        
        Input:
        @i: i-th position desired to change 0 in 1.
        '''
        if i>self.size:
            exit(0)
        octet_id = int(i//8)
        position_on_octet = i%8
        mask = 1 << position_on_octet
        self.array[octet_id] = self.array[octet_id] | mask
    
    def get_i(self, i):
        '''
        This function tests if the ith position contains a 1 to signify that our string is in the array.
        
        Input:
        @i: position desired to test if i is a 1.
        '''
        if i>self.size:
            exit(0)
        octet_id = int(i//8)
        position_on_octet = i%8
        mask = 1 << position_on_octet
        if self.array[octet_id]&mask != 0: return True
        else: return False

class BloomFilter :
    '''
    This class contains functions that generate a Bloom filter.
    '''
    def __init__(self, size, nb_hashs):
        '''
        This initialization function of class to create the Bloom filter using the BitArray (an array of n bits).
        
        Input:
        @size: Bloom filter size based on the size of an array of n bits.
        @nb_hashs: the number of hashes function executed.
        '''
        self.BA=BitArray(size)
        self.nb_hashs = nb_hashs
        self.count = 0

    def __len__(self):                      #A function that show Bloom filter size.
        return self.count
        
    def add_word(self, word):
        '''
        A function to add an element in the Bloom filter.
        
        Input:
        @word: element that we want to add in the Bloom filter.
        '''
        for i in range(self.nb_hashs):
            adress = kmer2hash(word, i)%self.BA.size
            self.BA.set_i(adress)
        self.count +=1
        
    def exists_word(self, word):
        '''
        A function to verify if an element is present or not in Bloom filter.
        
        Input:
        @word: element that we want to verify in Bloom filter.
        '''
        for i in range(self.nb_hashs):
            adress = kmer2hash(word,i)%self.BA.size
            if not self.BA.get_i(adress): return False
        return True
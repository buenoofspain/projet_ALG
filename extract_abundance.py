# -*- coding: utf-8 -*-
#! /bin/bash/env python

'''
@Subject: Projet ALG
@Authors: Aurélie Nicolas and Nicolas Guillaudeux
@Master Bio-informatique
@Program: Python 3.6.2

TO EXECUTE: 
>>> python3 extract_abundance.py -d "file with data reads" -o "output file result"
EXEMPLE : 
>>> python3 extract_abundance.py -d example/counted_31kmers_ecoli_sample_reads_snp.txt -o abundance.txt
'''

def extract_abundance(file_in, file_out):
    '''
    Function that extract abundance and count this abundance for each kmer.
    '''
	_dico = {}
	with open(file_in, 'r') as filin:
		with open(file_out, 'w') as filout:
			for line in filin:
				abondance = line.split()[0]
				if abondance in _dico:
					_dico[abondance] += 1
				else:
					_dico[abondance] = 1
			for abondance in _dico:
				filout.write(str(abondance) + "\t" + str(_dico[abondance]) + "\n")

def main():
	'''
	Main fuction to execute the project.
	'''	
	# Parser:'
	parser = argparse.ArgumentParser(prog = "extract_abundance.py")
	parser.add_argument("-d", "--reads_data", dest = "reads", metavar = "DATA_FILE", type = str, help = "Data file in input on .fasta, .fastq or .txt", required = True)
	parser.add_argument("-o", "--output", dest = "out", metavar = "OUTFILE", type = str, help = "Set of contigs in output", required = True)
	args = parser.parse_args()

	##Checking input arguments: 
	assert args.reads[-4:]=='.txt' or args.reads[-6:]=='.fasta' or args.reads[-7:]=='.fastaq', "Data files has not in '.fasta' or '.fastaq or '.txt'"

main()
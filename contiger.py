#! /bin/bash/env python
# -*- coding: utf-8 -*-

'''
@Subject: Projet ALG
@Authors: Aurélie Nicolas and Nicolas Guillaudeux
@Master Bio-informatique
@Program: Python 3.6.2

Command line test :
>>> python3 contiger.py -c mysterious_data/mysterious_reads.fasta.21-mer.txt -t mysterious_data/mysterious_k21_starters.fasta -o mysterious_data/contigs_results_k21 -f 100000000 -a 2 -n 7
'''

from BloomFilter import *
import time
import argparse
from pympler import asizeof


class Contiger:
	'''
	This class contains many functions that allow, from a starting file containing kmers and their abundance,
	to get the contig of a target kmer, its extension to the left and right.

	Contiger parameters:
		-	f: Bloom Filter size, by default is 1000000000 bits;
		-	a: Minimum abundance choose for counted kmers;
		-	n: Number of hash functions;
		-	k: Size of the kmers counted, unchangeable;
		- 	Size tips: >=k, unchangeable;
		-	Size bubble length: k, unchangeable;
		-	Width bubble size (branches number) : unmanaged.

	Input File:
	@targets_kmers_file: File with targets kmers to reconstruct contigs (one contig per one target kmer) in fasta format.
	@fasta_reads_file: File with kmer set with their abundance in text format (.txt).

	Output:
	@contig_file: File with contigs find in fasta format.
	@log_file : File with execution procedure summary.
	'''

	def __init__(self, bf_size, nb_hash, min_abund, kmer_size=0):
		'''
		A function to initialize necessary parameters for the Contiger class.
		'''
		self.bf_size = bf_size 		# Bloom filter size
		self.nb_hash = nb_hash 		# Hash functions number
		self.min_abund = min_abund 	# Abundance minimum to select counted kmers
		self.k = kmer_size			# To define kmer size
		self.nb = 0 				# Counter to know the number of kmer stored in the "de Bruijn Graph"

	def rev_comp(seq):
		'''
		A function to obtain the nucleotide sequence reverse complement.

		Input:
		@seq: the nucleotide sequence

		Output:
		The nucleotide sequence reverse complement for a future use.
		'''
		complementarity_dict = {'A':'T', 'T': 'A', 'G': 'C', 'C': 'G'} 	# Nucleotides complementarity in a dictionary format.
		return (''.join(complementarity_dict[c] for c in seq))[::-1]	# To return the reverse complement of the input nucleotide sequence.

	def canonical_representation(kmer):
		'''
		A function to select the sequence's canonical version between its forward version and its reverse complement version.
		
		Input:
		@kmer: the nucleotide sequence of the kmer tested.

		Output:
		The canonical version : either the forward version or the reverse complement version.
		'''
		reverse_complement = Contiger.rev_comp(kmer) 		# To create the kmer sequence reverse complement. 
		if (reverse_complement < kmer):						# To test the canonical version between forward and the reverse complement version.
			return reverse_complement						# If reverse complement is canonical : return reverse complement same the canonical version.
		return kmer 										# Else : return the forward version same the canonical version.

	def create_dBG(self, reads_file):
		'''
		A function to create an implicit de Bruijn Graph (dBG) with a Bloom filter. 
		Extracts kmers and their "abundances" in a dictionary.
		Stores kmers for a defined minimum abundance.
		Updates number variable showing the kmers stored number in dBG.

		Input: 
		@reads_file: file with set of kmer with their abundance in text format (.txt).
		@bf_size: Bloom filter size.
		@nb_hash: hash functions number.
		@min_abund: abundance minimum to select counted kmers.

		Output: 
		@dBG: the implicit de Bruijn Graph in a Bloom filter.
		@k: kmer size.
		'''
		print(">>> Bloom Filter size:", self.bf_size)						# To print Bloom filter size in terminal.
		filin = open(reads_file, 'r') 										# To open input file in text format.
		dBG = BloomFilter(self.bf_size, self.nb_hash)						# To initialize the Bloom filter with the Bloom filter size and hash functions number.
		for line in filin.readlines():										# To read each line of the input file.
			line = line.rstrip().upper().split() 							# To delete empty characters and carriage return. To make the case uniform. To create a list of 2 arguments : the number of the kmer tested and the kmer.
			if int(line[0])>=self.min_abund :								# For threshold choice: if kmer tested number is superior of threshold, we concerve it.
				dBG.add_word(Contiger.canonical_representation(line[1]))	# If kmer is concerved, we store it in the Bloom filter with the canonical representation.
				self.nb += 1 												# If kmer is store in the Bloom filter, we add +1 to count of kmers stored number.
				if self.k == 0:												# If kmer size is zero:
					self.k = len(line[1])									# We replace it by the real kmer size.
					print(">>> kmer size:", self.k)							# To print kmer size in terminal.
		print(">>> added", self.nb, "kmers")								# To print kmers number stored in the Bloom filter.
		filin.close()														# To close file.
		print(">>> dBG construction: OK")										# To print the function end in terminal.
		return dBG 															# To return the dBG for a use later.

	def get_right_neighbor(kmer, dBG):
		'''
		Return the set of right neighbors from a given kmer.

		Input:
		@kmer: the given kmer which we search the neighbors on the right.
		@dBG: the "De Bruijn graph".

		Output:
		@right_list: neighbors list on the right.
		'''
		right_list = []																# To define a set list of neighbors on the right of kmer tested. 
		for letter in ['A', 'C', 'G', 'T']:											# We test each possible neighbor nucleotide :
			if dBG.exists_word(Contiger.canonical_representation(kmer[1:]+letter)):	# if suffix with the tested nucleotide letter (a potential neighbor) exists (in the canonical version),
				right_list.append(letter)											# so, we add it to our neighbor list.
		return right_list															# To return neighbors list on the right.

	def get_left_neighbor(kmer, dBG):
		'''
		Return the set of left neighbor from a given kmer.

		Input:
		@kmer: the given kmer which we search neighbors on the left.
		@dBG: the "De Bruijn graph".

		Output:
		@left_list: neighbors list on the left.
		'''
		left_list = []																	# To define a set list of neighbors on the left of the kmer tested. 
		for letter in ['A', 'C', 'G', 'T']:												# We test each possible nucleotide :
			if dBG.exists_word(Contiger.canonical_representation(letter+kmer[0:-1])):	# if preffix (k-1 mer of the kmer) with the tested nucleotide letter (a potential neighbor) exists (in the canonical version),
				left_list.append(letter)												# So, we add it to our neighbor list.
		return left_list																# To return neighbors list on the left.

	def extend_right_kmer(kmer, dBG):
		'''
		A fonction to return the kmer with his right extension.
		It manages tips less than or equal to k and k bubble size.

		Input:
		@kmer: kmer tested which we want to rebuild a contig.
		@dBG: the "De Bruijn graph".

		Output:
		@unitig: unitig rebuilds to the right.
		'''
		unitig = kmer 													# Unitig is initialized with the kmer tested.
		while True:		
			successors = Contiger.get_right_neighbor(kmer, dBG)			# We are looking for neighbors list on the right.
			predecessors = Contiger.get_left_neighbor(kmer, dBG)		# And we are looking for neighbors list on the left.
			if len(predecessors) > 2: break								# If neighbors list size on the left is superior of 2, then we stop.

			elif len(predecessors) == 2:										# If neighbors list size on the left is equal to 2, we treat tip presence.
				""" Tip and bubble treatment on the left """
				count = 0														# We initialize a counter at zero to define tip size less or equal to k or bubble in k size.
				kmer_1 = predecessors[0] + kmer[:-1]							# We define the first neighbor in a variable kmer_1,
				kmer_2 = predecessors[1] + kmer[:-1]							# and the second in kmer_2 variable.
				while count <= len(kmer):										# Until the counter is less than or equal to k,
					predecessors_1 = Contiger.get_left_neighbor(kmer_1, dBG)	# We look at the predecessors neighbors of kmer_1
					predecessors_2 = Contiger.get_left_neighbor(kmer_2, dBG)	# and the predecessors neighbors of kmer_2.
					if len(predecessors_1) != 1 or len(predecessors_2) != 1:	# We check that one of them does not have one predecessor

						""" Tip treatment on the left """
						#We check that both have at most one predecessor and that the two are not null.
						if len(predecessors_1) <= 1 and len(predecessors_2) <= 1 and not (len(predecessors_1) == 0 and len(predecessors_2) == 0):
							# We select the correct kmer tested on both (the one that is not zero).
							good_predecessor = Contiger.remove_tips(dBG, predecessors_1, predecessors_2)
							kmer_1 = good_predecessor[0] + kmer_1[:-1]			# Thus we consider only the good kmer.
							kmer_2 = good_predecessor[0] + kmer_2[:-1]
						else: break												# Otherwise we stop the while loop.

						""" Bubble treatment on the left """
					elif len(predecessors_1) == 1 and len(predecessors_2) == 1:	# Other case, if both have only one predecessor,
						kmer_1 = predecessors[0] + kmer[:-1] 					# We continue to treat both.	
						kmer_2 = predecessors[1] + kmer[:-1]
					count += 1 													# We complete the counter to be able to delimit the condition of the while loop.
				if kmer_1 != kmer_2 and count==len(kmer_1): break				# If the kmer_1 and kmer_2 tested are different and we waited for the k size.

			""" Unitig extension """
			if len(successors) == 1:				# If neighbors list size on the right is egal to 1:
				kmer = kmer[1:] + successors[0] 	# We replace our kmer with his neighbor on the right.
				unitig += successors[0]				# We complete our unitig by adding his neighbor on the right.

			elif len(successors) == 2:											# If neighbors list size on the right is egal to 2:
				""" Tip and bubble treatment on the right """
				count = 0														# We initialize a counter at zero to define tip size less or equal to k or bubble in k size.
				kmer_1 = kmer[1:] + successors[0]								# We define a first kmer as the possible neighbor 1
				sequel_1 = successors[0]										# We define a first sequence with succession bases meet in bubble, we add the first neighbor
				kmer_2 = kmer[1:] + successors[1]								# We define a second kmer as the possible neighbor 2
				sequel_2 = successors[1]										# We define a second sequence with succession bases meet in bubble, we add the second neighbor
				while count <= len(kmer):										# As long as our test does not exceed the size of a kmer,
					successors_1 = Contiger.get_right_neighbor(kmer_1, dBG)		# We search neigbors of kmer_1 in a first variable
					successors_2 = Contiger.get_right_neighbor(kmer_2, dBG)		# And those of kmer_2 in a second variable
					if len(successors_1) != 1 and len(successors_2) != 1: 		# If there is not only one successor,

						""" Tip treatment on the right """
						#We check that both have at most one predecessor and that the two are not null.
						if len(successors_1) <= 1 and len(successors_2) <= 1 and not (len(successors_1) == 0 and len(successors_2) == 0):
							# We select the correct kmer tested on both (the one that is not zero).	
							good_successor = Contiger.remove_tips(dBG, successors_1, successors_2)
							kmer_1 = kmer_1[1:] + good_successor[0] 	# Thus we consider only the good kmer.
							kmer_2 = kmer_2[1:] + good_successor[0] 	# Thus we consider only the good kmer.
							sequel_1 += good_successor[0]				# We complete our sequence by the good successor
							sequel_2 += good_successor[0]
						else: break										# Otherwise we stop the while loop.

						""" Bubble treatment on the right """
					elif len(successors_1) == 1 and len(successors_2) == 1: # Other case, if both have only one successor,
						kmer_1 = kmer_1[1:] + successors_1[0]			# If condition is fulfilled, continue the search with the following
						kmer_2 = kmer_2[1:] + successors_2[0]
						sequel_1 += successors_1[0]						# And we expanded our sequence with the base of kmer's neighbor
						sequel_2 += successors_2[0]
					count += 1 										# At the end, we add 1 to counter to complete the output condition.
				if sequel_1[1:] != sequel_2[1:]: break				# When we reach the size of a kmer, we check that the end of the two sequences is identical: it means that we have left the bubble.
				successors_1 = Contiger.get_right_neighbor(sequel_1[-len(kmer):], dBG)	# We look at the neighbors for our sequence 1 with the size of K for the end of the sequence
				if len(successors_1) != 1 : break					# If we have more than one successor, we stop;
				kmer = sequel_1[-len(kmer)+1:] + successors_1[0]	# Else, we use neighbor and
				unitig += sequel_1 + successors_1[0]				# Extend unitig.
			else: break												# If we do not have one or two successors, we stop.
		return unitig 												# At the end, we return complete unitig.
    
	def extend_left_kmer(kmer, dBG):
		'''
		A function to return kmer with its left extension.

		Input:
		@kmer: kmer tested which we want to rebuid a unitig.
		@dBG: the "De Bruijn graph".

		Output:
		@unitig: the reconstructed unitig.
		'''
		# We do reverse complement of the tested kmer, then we realize its extension to the right and we reverse complement to get the final extension to the left of kmer tested.
		return Contiger.rev_comp(Contiger.extend_right_kmer(Contiger.rev_comp(kmer), dBG)) 

	def generate_contig(self, kmer, dBG):
		'''
		A function to generate a contig from a kmer tested.

		Input:
		@kmer: a kmer which we are trying to generate a unitig.
		@dBG: the "De Bruijn graph".

		Output:
		@contig: the assembly of the contigs left and right.
		'''
		right_unitig = Contiger.extend_right_kmer(kmer, dBG) 	# To get kmer's right extension.
		left_unitig = Contiger.extend_left_kmer(kmer, dBG)		# To get kmer's left extension.
		contig = left_unitig+right_unitig[len(kmer):]			# Fusion of right and left sides to form the largest unitig.
		return contig 											# To return the unitig formed.

	def remove_tips(dBG, neighbor_1, neighbor_2):
		'''
		A function that will remove the tips.

		Input:
		@dBG: the "De Bruijn graph".
		@neighbor_1: First sequence in the loop.
		@neighbor_2: Second sequence in the loop.

		Output:
		The sequence that is not a tip.
		'''
		if len(neighbor_1) == 1: return neighbor_1	# If the first has only one neighbor, then we return it
		else: return neighbor_2						# Otherwise we return the other.


if __name__ == '__main__':
	'''
	Main fuction to execute the project.
	'''	
	### Parser:
	parser = argparse.ArgumentParser(prog = "contiger.py")

	### Optional input parameters
	# Optionnal argument for Bloom filter size, minimum abundance kmers, hash functions number.
	parser.add_argument("-f", "--len_BF", dest = "BF", metavar = "BLOOM_FILTER_SIZE", type = int, default = 1000000000, help = "Bloom Filter size, by default is 1000000000 bits. The format is an integer.")
	parser.add_argument("-a", "--abund", dest = "min_abund", metavar = "MIN_ABUND_KMER", type = int, default = 2, help = "Minimum abundance choose for counted kmers. The format is an integer.")
	parser.add_argument("-n", "--nb_hash", dest = "nb_hash", metavar = "NB_HASH_FUNCTION", type = int, default = 7, help = "Number of hash functions. The format is an integer.")

	### Input required parameters
	# Required parameters for files with targets kmers and kmers counted from reads.
	parser.add_argument("-t", "--target_kmer", dest = "kmer", metavar = "TARGET_KMER_FILE", type = str, help = "Targets kmers file in input on .fasta", required = True)
	parser.add_argument("-c", "--count_kmer", dest = "reads", metavar = "COUNT_KMER_READS_FILE", type = str, help = "Counted kmers data file in input on .fasta, .fastq or .txt", required = True)

	### Output required parameters
	# Required parameters to generate a output fasta file with contigs from targets kmers and for log file.
	parser.add_argument("-o", "--output", dest = "out", metavar = "OUTFILE", type = str, help = "Set of contigs in an output file. Enter the file name desired without suffix that will be added in '.fasta' format." , required = True)
	
	### Args definition
	args = parser.parse_args()

	### Checking input format arguments: 
	# To check targets kmers file in fasta format and kmers counted from reads in text format.
	assert args.kmer[-6:]=='.fasta', "Targets kmers file has not in '.fasta'"
	assert args.reads[-4:]=='.txt', "Kmers counted from reads file has not in '.txt'"

	### To write the log file.
	with open(args.out+".log", 'w') as log_file:
		log_file.write("\t**************************\n\t***** START PROGRAMM *****\n")

		# To launch calculation time
		t0 = time.time()

		# To construct contiger with the right parameters.
		ctgr = Contiger(args.BF, args.nb_hash, args.min_abund)

		# To construct de Bruijn Graph
		log_file.write("\nConstruction of the 'De Bruijn graph':\n")
		dBG = ctgr.create_dBG(args.reads)
		log_file.write(">>> dBG construction: OK\n")
		
		# To write kmers number added in the "De Bruijn graph".
		log_file.write(str(">>> Added " + str(dBG.count) + " kmers\n"))

		# To print, in the log, parameters set used for contiger.
		log_file.write("\nParameters used for contiger:\n")
		
		# To print kmer size and bubble size arising from it.
		log_file.write(">>> Default parameter for kmers size: " + str(ctgr.k) + "\n")
		log_file.write(">>> Default parameter for bubble size: one kmer's size: " + str(ctgr.k) + "\n")

		# To print Bloom Filter size.
		log_file.write(">>> Default parameter for Bloom Filter size: 100000000 bits\n")
		log_file.write(">>> Parameter used for Bloom Filter size: " + str(args.BF) + " bits\n")

		# To print the minimum abundance for counted kmers.
		log_file.write(">>> Default parameters for minimum abundance for counted kmers: 2\n")
		log_file.write(">>> Parameters used for minimum abundance for counted kmers: "+str(args.min_abund)+"\n")

		# To print the hash functions number for the Bloom filter.
		log_file.write(">>> Default parameters for hash functions number: 7\n")
		log_file.write(">>> Parameters used for hash functions number: " + str(args.nb_hash) + "\n")

		# To print, in th log, data used for contiger
		log_file.write("\nInput used for contiger and output created:\n")
		
		# To print input counted kmers data file, then input starters file and final the output name file.
		log_file.write(">>> Counted kmers data file: "+str(args.reads)+"\n")
		log_file.write(">>> Targets kmers file: "+str(args.kmer)+"\n")
		log_file.write(">>> Output file name: "+str(args.out)+".fasta\n")

		# To print results.
		log_file.write("\nResults:")
		with open(args.kmer, 'r') as targets_kmers:								# We read targets kmers file.
			number_contig = 1 													# We define a counter to 1 to decide the name of result contig generates.
			with open(args.out+".fasta", 'w') as output_fasta_file:				# We create output file to store contigs name and contigs sequence in fasta format.
				lines = targets_kmers.readlines()								# We read lines of the file.
				for line in lines:												# For each line,
					line = line.rstrip().upper() 								# To delete empty characters and carriage return. To make the case uniform.
					if line.startswith('>'): continue							# If line starts with '>' character, do not consider it.
					contig = ctgr.generate_contig(line, dBG) 					# To generate contig
					parameters_contig = str("> contig " + str(number_contig))	# To define the name for the fasta file and the track in log file. 
					log_file.write(str("\n" + parameters_contig))				# To write the contig name in fasta format in log file.
					log_file.write(str("\nSize of the contig create: " + str(len(contig))))	# To write contig size in log file.
					output_fasta_file.write(parameters_contig)					# To write the contig name in fasta format in output fasta file.
					output_fasta_file.write("\n" + contig + "\n")				# To write the contig sequence in the output fasta file.
					number_contig += 1 											# To add 1 in the counted to define the next number of the next target kmer.
				log_file.write("\n>>> Contig generation: OK")
				print("\n>>> Contig generation: OK") 								# To print the end of the contig generation in terminal.

		# To end of calculation time
		log_file.write("\n\nTime for the execution programm: ")
		time_of_programm = (time.time()-t0)
		print(">>> Execution time: " + str(time_of_programm)) 	# To print the execution time in terminal.
		log_file.write(str(time_of_programm)+" sec.\n")         # To print the execution time in log file.

		# To print the maximum memory used
		log_file.write("\nMemory mesure: ")
		log_file.write("De Bruijn graph = "+str(asizeof.asizeof(dBG))+" bytes\n")
		log_file.write("\n\t***** END PROGRAMM *****\n\t************************")
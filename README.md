# ALG project - README

## Developers 
Aurélie Nicolas and Nicolas Guillaudeux, students in Master's degree in Bioinformatics (University of Rennes 1).

## Description
This document present the developper programm for the ALG project.
ALG it is a teaching unit of the Master of Bioinformatics at the University of Rennes 1, under the supervision of P. Peterlongo and C. Lemaitre.

## Contiger execution
To execute the contiger with example, used the following command line: 

    # Line code explication:
	>>> python3 contiger.py -c mysterious_data/mysterious_reads.fasta.21-mer.txt -t mysterious_data/ mysterious_k21_starters.fasta -o mysterious_data/contigs_results_k21 -f 100000000 -a 2 -n 7
	>>> python3 contiger.py --count_kmer mysterious_data/mysterious_reads.fasta.21-mer.txt --target_kmer mysterious_data/mysterious_k21_starters.fasta --output mysterious_data/contigs_results_k21 --len_BF 1000000000 --abund 2 --nb_hash 7

Example:

    # Line code example: 
	>>> python3 contiger.py -c mysterious_data/mysterious_reads.fasta.21-mer.txt -t mysterious_data/mysterious_k21_starters.fasta -o mysterious_data/contigs_results_k21 
	>>> python3 contiger.py -c mysterious_data/mysterious_reads.fasta.31-mer.txt -t mysterious_data/mysterious_k31_starters.fasta -o mysterious_data/contigs_results_k31
	>>> python3 contiger.py -c mysterious_data/mysterious_reads.fasta.51-mer.txt -t mysterious_data/mysterious_k51_starters.fasta -o mysterious_data/contigs_results_k51
	>>> python3 contiger.py -c mysterious_data/mysterious_reads.fasta.71-mer.txt -t mysterious_data/mysterious_k71_starters.fasta -o mysterious_data/contigs_results_k71

## Mapper execution
To execute the mapper with example, used the following comman line:

	# Line code explication:
	>>> python3 mapper.py -c "file with contigs" -g “The reference genome” -k “Seed size”
	>>> python3 contiger.py --contig "file with contigs" --genome "The reference genome" --len_k “Seed size”

Example:

	# Line code example:
	>>> python3 mapper.py -c mysterious_data/contigs_results_k21.fasta -g mysterious_data/mysterious_reference.fasta
	>>> python3 mapper.py -c mysterious_data/contigs_results_k31.fasta -g mysterious_data/mysterious_reference.fasta
	>>> python3 mapper.py -c mysterious_data/contigs_results_k51.fasta -g mysterious_data/mysterious_reference.fasta
	>>> python3 mapper.py -c mysterious_data/contigs_results_k71.fasta -g mysterious_data/mysterious_reference.fasta

## Files used for the projects
- a python script of the Bloom filter: [BloomFilter.py](BloomFilter.py)
- a python script of the dynamic matrix: [DynamicMatrix.py](DynamicMatrix.py)
- a python script of the contigueur: [contiger.py](contiger.py)
- a python script of the mappeur: [mapper.py](mapper.py)

## Gitlab of the project
https://gitlab.com/buenoofspain/projet_ALG
